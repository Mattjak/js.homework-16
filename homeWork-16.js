let factorial = prompt("Please enter a digit");
while (isNaN(factorial)) {
    factorial = prompt("Please enter a digit");
}
function calcFact(factorial)
{
    if (factorial === 0) {
        return 1;
    }
    else {
        return factorial * calcFact( factorial - 1 );
    }
}
console.log(calcFact(factorial));